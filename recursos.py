# Autora: Martha Cango
# Email : martha.cango@unl.edu.ec

# Crear un programa en el cual se permita crear un Cuestionario que contenga al
# menos 3 preguntas de selección en base al modelo de clases codificado en el
# item 1. Presentar al final todo lo creado.


class Cuestionario:
    identificador = ""
    nombre = ""
    preguntas = []

    def __init__(self,identificador, nombre):
        self.identificador = identificador
        self.nombre = nombre
        self.preguntas = []


class PreguntaSeleccion:
    numero = 0
    texto = ""
    opcion1 = ""
    opcion2 = ""
    opcion3 = ""
    respuesta = ""

    def __init__(self, numero, texto):
        self.numero = numero
        self.texto = texto
        self.opcion1 = ""
        self.opcion2 = ""
        self.opcion3 = ""
        self.respuesta = ""

    def __str__(self):
        return self.numero + "." + self.texto


pregunta_1 = Cuestionario("Pregunta Nº1:", "Definición de Escritura.")
print(pregunta_1.identificador)
print(pregunta_1.nombre)
pregunta_1.preguntas = ["Subrayar la respuesta correcta:"]
print(pregunta_1.preguntas)
opcion_1 = PreguntaSeleccion("1.", "La escritura es un sistema de representación gráfica de un idioma.")
print(opcion_1.numero, opcion_1.texto)
opcion_2 = PreguntaSeleccion("2.", "La escritura es una herramienta que no ayuda a comunicarse.")
print(opcion_2.numero, opcion_2.texto)
opcion_3 = PreguntaSeleccion("3.", "La escritura nos ayuda a comunicarnos solo en un papel.")
print(opcion_3.numero, opcion_3.texto)


pregunta_2 = Cuestionario("Pregunta Nº2:", "Origen de la Escritura.")
print(pregunta_2.identificador)
print(pregunta_2.nombre)
pregunta_2.preguntas = ["Seleccionar la respuesta correcta:"]
print(pregunta_2.preguntas)
item_1 = PreguntaSeleccion("1.", "Inicia en Mesopotamia hace 4000 ac.")
print(item_1.numero, item_1.texto)
item_2 = PreguntaSeleccion("2.", "Inicia en China hace 3000 ac.")
print(item_2.numero, item_2.texto)
item_3 = PreguntaSeleccion("3.", "Ninguna de las anteriores.")
print(item_3.numero, item_3.texto)


pregunta_3 = Cuestionario("Pregunta Nº3:", "Tipos de Escritura.")
print(pregunta_3.identificador)
print(pregunta_3.nombre)
pregunta_3.preguntas = ["Elegir las opciones correctas sobre los tipos de Escritura:"]
print(pregunta_3.preguntas)
alt_1 = PreguntaSeleccion("1.", "Pictográfica")
print(alt_1.numero, alt_1.texto)
alt_2 = PreguntaSeleccion("2.", "Ideográfica")
print(alt_2.numero, alt_2.texto)
alt_3 = PreguntaSeleccion("3.", "Silábica")
print(alt_3.numero, alt_3.texto)
